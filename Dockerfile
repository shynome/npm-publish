FROM node:carbon-alpine

ENV NODE_ENV=production

WORKDIR /npm-publish

COPY npm_deps.json /npm-publish/package.json

RUN npm i && rm -rf package*.json

COPY shynome-npm-publish-*.tgz /tmp/pack.tgz

RUN npm i /tmp/pack.tgz \
    && rm /tmp/pack.tgz 

CMD [ "node", "-e", "require('@shynome/npm-publish')" ]